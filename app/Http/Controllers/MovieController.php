<?php

namespace App\Http\Controllers;

use App\Models\Movie;

class MovieController extends Controller
{
    public function index()
    {
        $movies = Movie::paginate(6);

        return view('movies.index', compact('movies'));
    }

    public function show(Movie $movie)
    {
        return view('movies.details', compact('movie'));
    }
}
