<?php

namespace App\Http\Controllers;

use App\Models\Genre;

class GenreController extends Controller
{
    public function movies(Genre $genre)
    {
        $movies = $genre->movies()->paginate(6);

        return view('genres.movies', [
            'genre' => $genre,
            'movies' => $movies
        ]);
    }
}
