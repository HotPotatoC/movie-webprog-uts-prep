<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Movie;



class HomeController extends Controller
{
    public function index()
    {
        $movies = Movie::paginate(6);
        $genres = Genre::all();

        return view('home', [
            'movies' => $movies,
            'genres' => $genres
        ]);
    }
}
