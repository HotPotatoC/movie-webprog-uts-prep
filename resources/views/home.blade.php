<x-layout title="Home">
    <div class="container">
        <div class="row">
            <div class="d-flex flex-wrap">
                @forelse ($genres as $genre)
                <h5 class="me-3">
                    <a href="{{ route('genre.movies', $genre->id) }}"
                        class="focus-ring badge bg-white text-dark text-decoration-none">{{ $genre->name }}</a>
                </h5>
                @empty
                <div class="col-md-3">
                    <h2>No Genres...</h2>
                </div>
                @endforelse
            </div>
        </div>
        <div class="row">
            @forelse ($movies as $movie)
            <div class="col-md-4 mb-5">
                <a href="{{ route('movies.show', $movie->id) }}">
                    <div class="card border-dark">
                        <img class="card-img-top" src="{{ $movie->photo }}" alt="{{ $movie->title }} Image">
                        <div class="card-img-overlay h-100 d-flex flex-column justify-content-end">
                            <h3 class="card-title text-white">{{ $movie->title }}</h3>
                        </div>
                    </div>
                </a>
            </div>
            @empty
            <div class="col-md-3">
                <h2>No Movies...</h2>
            </div>
            @endforelse
        </div>
        <div class="d-flex">
            {{$movies->links()}}
        </div>
    </div>
</x-layout>
