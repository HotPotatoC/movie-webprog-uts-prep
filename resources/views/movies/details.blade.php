<x-layout title="{{$movie->title}} Details">
    <div class="container">
        <div class="row">
            <div class="col">
                <img src="{{$movie->photo}}" alt="" />
            </div>
            <div class="col text-white">
                <h1>{{ $movie->title }}</h1>
                <h3>{{$movie->rating}}</h3>
                <div class="d-flex flex-wrap">
                    @forelse ($movie->genres as $genre)
                    <h5 class="me-3">
                        <a href="{{ route('genre.movies', $genre->id) }}"
                            class="focus-ring badge bg-white text-dark text-decoration-none">{{ $genre->name
                            }}</a>
                    </h5>
                    @empty
                    <div class="col-md-3">
                        <h2>No Genres...</h2>
                    </div>
                    @endforelse
                </div>
                <p>{{$movie->description}}</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <h3 class="text-white">{{count($movie->episodes)}} Episodes</h3>
            </div>
            @forelse ($movie->episodes as $episode)
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-baseline text-white">
                    <h1>{{$episode->episode}}</h1>
                    <h1>{{$episode->title}}</h1>
                </div>
            </div>
            @empty
        </div>
        <div class="row">
            <h2>No Episodes...</h2>
        </div>
        @endforelse
    </div>
</x-layout>
