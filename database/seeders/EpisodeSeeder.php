<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EpisodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movieIds = DB::table("movies")->pluck("id");
        foreach ($movieIds as $movie) {
            for ($i = 0; $i < 24; $i++) {
                DB::table('episodes')->insert([
                    'movie_id' => $movie,
                    'episode' => $i + 1,
                    'title' => fake()->word,
                ]);
            }
        }
    }
}
