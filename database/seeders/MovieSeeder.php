<?php

namespace Database\Seeders;

use App\Models\Movie;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movies')->insert([
            "title" => "HARAKIRI",
            "photo" => fake()->imageUrl,
            "description" => "A ronin (Tatsuya Nakadai) asks to commit hara-kiri in the courtyard of a feudal lord's estate to regain his honor.",
            "rating" => rand(1, 5),
        ]);

        DB::table('movies')->insert([
            "title" => "THE GODFATHER",
            "photo" => fake()->imageUrl,
            "description" => "An organized-crime dynasty's aging patriarch (Marlon Brando) transfers control to his reluctant son (Al Pacino).",
            "rating" => rand(1, 5),
        ]);

        DB::table('movies')->insert([
            "title" => "THE GODFATHER PART II",
            "photo" => fake()->imageUrl,
            "description" => "Michael Corleone (Al Pacino) expands his family's crime empire while his father's old foe (Robert De Niro) tries to escape a life of poverty.",
            "rating" => rand(1, 5),
        ]);

        DB::table('movies')->insert([
            "title" => "WHIPLASH",
            "photo" => fake()->imageUrl,
            "description" => "A jazz drummer (Miles Teller) seeks greatness under a ruthless instructor (J.K. Simmons).",
            "rating" => rand(1, 5),
        ]);

        DB::table('movies')->insert([
            "title" => "THE DARK KNIGHT",
            "photo" => fake()->imageUrl,
            "description" => "Batman (Christian Bale) battles a vicious criminal known as the Joker (Heath Ledger).",
            "rating" => rand(1, 5),
        ]);

        DB::table("movies")->insert([
            "title" => "Spirited Away",
            "photo" => fake()->imageUrl,
            "description" => "A young girl, Chihiro, becomes trapped in a strange new world of spirits. When her parents undergo a mysterious transformation, she must call upon the courage she never knew she had to free her family.",
            "rating" => rand(1, 5),
        ]);

        DB::table("movies")->insert([
            "title" => "Monster",
            "photo" => fake()->imageUrl,
            "description" => "Kenzou Tenma, a Japanese brain surgeon in Germany, finds his life in utter turmoil after getting involved with a psychopath that was once a former patient.",
            "rating" => 5,
        ]);

        DB::table("movies")->insert([
            "title" => "Neon Genesis Evangelion",
            "photo" => fake()->imageUrl,
            "description" => "Kenzou Tenma, a Japanese brain surgeon in Germany, finds his life in utter turmoil after getting involved with a psychopath that was once a former patient.",
            "rating" => 5,
        ]);

        Movie::factory()->count(25)->create();

        $genres = DB::table("genres")->pluck("id");
        $movies = DB::table("movies")->pluck("id");
        foreach ($movies as $movie) {
            $genreIds = $genres->random(4);
            foreach ($genreIds as $genreId) {
                DB::table("genre_movies")->insert([
                    "movie_id" => $movie,
                    "genre_id" => $genreId,
                ]);
            }
        }
    }
}
